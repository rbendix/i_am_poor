import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("I Am Poor"),
            backgroundColor: Colors.deepPurple,
          ),
          backgroundColor: Colors.purple,
          body: Center(
            child: Image(
              image: AssetImage("images/coal.png")
            ),
          )
      ),
    ),
  );
}
